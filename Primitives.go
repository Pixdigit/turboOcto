package turboOcto

import (
	"github.com/veandco/go-sdl2/sdl"
	geo "gitlab.com/Pixdigit/geometry"
)

type Rect struct {
	*geo.Rect
	// COMBAK: are constrains really a good idea?
	constraint func(*Rect) error
}

func NewRect(p geo.Point, w, h int) (*Rect, error) {
	geoR := geo.NewRect(p, geo.Scalar(w), geo.Scalar(h))
	r := &Rect{}
	r.Rect = &geoR

	return r, nil
}
func NewRectFromGeoRect(r geo.Rect) (*Rect, error) {
	rect := &Rect{&r, func(r *Rect) error { return nil }}
	return rect, nil
}

func (r *Rect) IsClicked(which buttonPosition) bool {
	return r.HasMouseState(which, PRESSING)
}
func (r *Rect) HasMouseState(which buttonPosition, state buttonState) bool {
	// We can safely ignore errors. In doubt no collision is happening
	collision, _ := r.CollidesWith(Mouse.Pos)
	return collision && (*Mouse.Buttons[which] == state)
}

func (r *Rect) SetConstraint(constraint func(*Rect) error) error {
	r.constraint = constraint
	r.constraint(r)
	return nil
}

func (r *Rect) toSDLRect() (*sdl.Rect, error) {
	topLeft := r.Ref()
	SDLRect := &sdl.Rect{int32(topLeft.X), int32(topLeft.Y), int32(r.W), int32(r.H)}
	return SDLRect, nil
}
