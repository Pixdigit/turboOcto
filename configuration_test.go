package turboOcto

import (
	"io/ioutil"
	"os"
	"testing"

	tools "gitlab.com/Pixdigit/goTestTools"
)

func TestDeserialization(t *testing.T) {

	exampleConf := []byte(`
    {
       "AllowFrameSkipping": false,
       "DefaultSpriteTimerMode": 0,
       "ResourcePath": "./",
       "UpdateOnRefresh": true,
       "Internal": {
          "Fullscreen": false,
          "GrabInput": false,
          "WindowSize": {
        	"W": 50,
        	"H": 50
          },
          "VRes": {
        	"W": 768,
        	"H": 1366
          }
       }
    }`)

	file, err := ioutil.TempFile("", "")
	defer func() {
		file.Close()
		os.Remove(file.Name())
	}()
	if err != nil {
		tools.WrapErr(err, "could not create temporary test file", t)
	}
	tmpFilePath := file.Name()
	file.Write(exampleConf)

	err = SetDefaultConf()
	if err != nil {
		tools.WrapErr(err, "could not load default configuration", t)
	}

	err = LoadConf(tmpFilePath)
	if err != nil {
		tools.WrapErr(err, "could not unmarshal example config", t)
	}
	err = updateFromInternalCfg()
	if err != nil {
		tools.WrapErr(err, "could not update based on conf", t)
	}
	if err != nil {
		tools.WrapErr(err, "failed to load configuration", t)
	}

	tools.Test(!AllowFrameSkipping, "default configuration did not change", t)
	tools.Test(UpdateOnRefresh, "default configuration did not change", t)
	tools.Test(DefaultSpriteTimerMode == 0, "default configuration did not change", t)

	tools.Test(!isFullscreen, "internal configuration did not change", t)
	t.Log(windowSize)
	tools.Test(windowSize.W == 50, "internal configuration did not change", t)
	tools.Test(windowSize.H == 50, "internal configuration did not change", t)
	t.Log(vRes)
	tools.Test(vRes.W == 768, "internal configuration did not change", t)
	tools.Test(vRes.H == 1366, "internal configuration did not change", t)
	//TODO: test saving

}
