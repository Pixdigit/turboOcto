package turboOcto

type cfgJSON struct {
	ConfigPath              string
	AllowFrameSkipping      bool
	DefaultFontRenderMethod renderMethod
	DefaultSpriteTimerMode  timerMode
	FramesVisibleOnLoad     bool
	ResourcePath            string
	SaveOnQuit              bool
	UpdateOnRefresh         bool

	Internal internalCfgJSON
}

type internalCfgJSON struct {
	IsFullscreen bool
	GrabInput    bool
	WindowSize   Resolution
	VRes         Resolution
}

func newCfgJSON() cfgJSON {
	return cfgJSON{
		ConfigPath,
		AllowFrameSkipping,
		DefaultFontRenderMethod,
		DefaultSpriteTimerMode,
		FramesVisibleOnLoad,
		ResourcePath,
		SaveOnQuit,
		UpdateOnRefresh,
		internalCfgJSON{
			isFullscreen,
			grabInput,
			windowSize,
			vRes,
		},
	}
}

func (cfg *cfgJSON) sanityCheck() {
	if cfg.DefaultSpriteTimerMode != USE_FRAME_COUNT && cfg.DefaultSpriteTimerMode != USE_TIME_PASSED {
		cfg.DefaultSpriteTimerMode = USE_FRAME_COUNT
	}
	if cfg.Internal.WindowSize == (Resolution{0, 0}) {
		quaterSize := screenSize
		quaterSize.W /= 2
		quaterSize.H /= 2
		cfg.Internal.WindowSize = quaterSize
	}
	if cfg.Internal.VRes == (Resolution{0, 0}) {
		cfg.Internal.VRes = cfg.Internal.WindowSize
	}

	method := cfg.DefaultFontRenderMethod
	if method != BLENDED && method != SHADED && method != SOLID {
		cfg.DefaultFontRenderMethod = BLENDED
	}
}

func (cfg cfgJSON) export() {
	AllowFrameSkipping = cfg.AllowFrameSkipping
	ConfigPath = cfg.ConfigPath
	DefaultFontRenderMethod = cfg.DefaultFontRenderMethod
	DefaultSpriteTimerMode = cfg.DefaultSpriteTimerMode
	FramesVisibleOnLoad = cfg.FramesVisibleOnLoad
	ResourcePath = cfg.ResourcePath
	SaveOnQuit = cfg.SaveOnQuit
	UpdateOnRefresh = cfg.UpdateOnRefresh

	isFullscreen = cfg.Internal.IsFullscreen
	grabInput = cfg.Internal.GrabInput
	windowSize = cfg.Internal.WindowSize
	vRes = cfg.Internal.VRes
}
