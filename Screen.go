package turboOcto

import (
	"github.com/pkg/errors"
	"github.com/veandco/go-sdl2/img"
	"github.com/veandco/go-sdl2/sdl"
)

//Finds usage also elsewhere
type Renderable interface {
	render() error
}

var screenSize Resolution
var windowSize Resolution
var canvasSize Resolution
var vRes Resolution
var frameCount int32
var isFullscreen bool

var grabInput bool

var screenRenderer *sdl.Renderer
var window *sdl.Window
var displayIndex int //TODO: Dynamically update when window moved

var rmask uint32 = 0x000000ff
var gmask uint32 = 0x0000ff00
var bmask uint32 = 0x00ff0000
var amask uint32 = 0xff000000

func initializeGraphics() (err error) {
	//Default is LIL_ENDIAN
	if sdl.BYTEORDER == sdl.BIG_ENDIAN {
		rmask = 0xff000000
		gmask = 0x00ff0000
		bmask = 0x0000ff00
		amask = 0x000000ff
	}

	//Create graphical interfaces
	windowFlags := uint32(sdl.WINDOW_SHOWN) | uint32(sdl.WINDOW_FULLSCREEN_DESKTOP)
	window, err = sdl.CreateWindow("", sdl.WINDOWPOS_UNDEFINED, sdl.WINDOWPOS_UNDEFINED, 0, 0, windowFlags);	if err != nil {return errors.Wrap(err, "could not create window")}
	window.SetGrab(true)

	screenRenderer, err = sdl.CreateRenderer(window, -1, sdl.RENDERER_PRESENTVSYNC);	if err != nil {return errors.Wrap(err, "could not create screenRenderer")}

	displayIndex, err := window.GetDisplayIndex();	if err != nil {return errors.Wrap(err, "could not get display index")}
	dmode, err := sdl.GetDesktopDisplayMode(displayIndex);	if err != nil {return errors.Wrap(err, "could not get display mode")}

	//initialize sizes
	screenSize = Resolution{int(dmode.W), int(dmode.H)}
	w, h := window.GetSize()
	windowSize = Resolution{int(w), int(h)}
	w, h, err = screenRenderer.GetOutputSize();	if err != nil {return errors.Wrap(err, "could not read output size")}
	canvasSize = Resolution{int(w), int(h)}
	vRes = canvasSize

	if isFullscreen {
		Fullscreen()
	} else {
		Windowed()
		SetWindowSize(screenSize.W/2, screenSize.H/2)
	}

	Clear()
	return nil
}

func getSDLSize(res Resolution) (int32, int32) {
	return int32(res.W), int32(res.H)
}

func SetDecoration(title string, iconPath string) error {
	window.SetTitle(title)
	iconPath = ResourcePath + iconPath
	if iconPath != "" {
		if exists, err := pathExists(iconPath); err != nil {
			return errors.Wrap(err, "could not check wether icon file exists")
		} else if !exists {
			return errors.New("path to icon does not exist")
		} else {
			icon, err := img.Load(iconPath);	if err != nil {return errors.Wrap(err, "could not load icon from path")}
			window.SetIcon(icon)
		}
	}
	return nil
}

func Fullscreen() error {
	window.SetSize(getSDLSize(screenSize))
	window.SetFullscreen(sdl.WINDOW_FULLSCREEN)
	canvasSize = screenSize
	FillScreen(0, 0, 0, 0)
	Clear()
	isFullscreen = true
	return nil
}
func Windowed() error {
	const SDL_WINDOW_WINDOWED = 0
	window.SetFullscreen(SDL_WINDOW_WINDOWED)
	window.SetSize(getSDLSize(windowSize))
	window.SetPosition(sdl.WINDOWPOS_CENTERED, sdl.WINDOWPOS_CENTERED)
	canvasSize = windowSize
	Clear()
	isFullscreen = false
	return nil
}

func SetVirtualSize(w, h int) error {
	vRes = Resolution{w, h}
	screenRenderer.SetLogicalSize(getSDLSize(vRes))
	err := Clear();	if err != nil {return errors.Wrap(err, "could not clear window after changing virtual size")}
	return nil
}
func SetWindowSize(w, h int) error {
	windowSize = Resolution{w, h}
	if !isFullscreen {
		window.SetSize(getSDLSize(windowSize))
	}
	return nil
}
func WindowSize() Resolution {
	return windowSize
}
func VRes() Resolution {
	return vRes
}

func FillScreen(r, g, b, a uint8) error {
	err := screenRenderer.SetDrawColor(r, g, b, a);	if err != nil {return errors.Wrap(err, "could not set draw color for fill operation")}
	err = screenRenderer.FillRect(nil);	if err != nil {return errors.Wrap(err, "could not execute fill operation")}
	return nil
}
func Clear() error {
	return FillScreen(0, 0, 0, 0)
}
func Render() []error {
	errs := []error{}
	for _, elem := range zSpace.Elems() {
		err := elem.(Renderable).render()
		errs = append(errs, errors.Wrap(err, "error while rendering"))
	}

	screenRenderer.Present()
	frameCount++
	//Clear up dirty frameBuffer
	err := Clear()
	if err != nil {
		errs = append(errs, errors.Wrap(err, "error while clearing frame"))
	}
	if len(errs) >= 1 {
		return errs
	}

	return []error{nil}
}
