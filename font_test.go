package turboOcto

import (
	"testing"

	"github.com/veandco/go-sdl2/sdl"
	tools "gitlab.com/Pixdigit/goTestTools"
)

func TestFontRenderMethods(t *testing.T) {
	SetWindowSize(500, 500)
	Windowed()

	testText := "blablabla"
	f, err := OpenFont("./assets/fonts/aileron/Aileron-Black.otf", 28)
	if err != nil {
		tools.WrapErr(err, "could not open font", t)
	}
	f.ColorRed = 0
	f.ColorGreen = 255
	f.ColorBlue = 255
	f.ColorAlpha = 255
	frame, err := f.RenderString(testText)
	AddElement(frame, 1)
	if err != nil {
		tools.WrapErr(err, "could not render blended with font", t)
	}

	f.Method = SHADED
	f.ColorRed = 255
	f.ColorGreen = 0
	f.ColorBlue = 255
	f.ColorAlpha = 180
	frame2, err := f.RenderString(testText)
	if err != nil {
		tools.WrapErr(err, "could not render shaded with font", t)
	}
	AddElement(frame2, 1)

	f.Method = SOLID
	f.ColorRed = 255
	f.ColorGreen = 255
	f.ColorBlue = 0
	f.ColorAlpha = 100
	frame3, err := f.RenderString(testText)
	if err != nil {
		tools.WrapErr(err, "could not render solid with font", t)
	}
	AddElement(frame3, 1)

	Update()

	if !testing.Short() {
		window.SetTitle("Check fonts")
		sdl.Delay(3000)
	}
}
