package turboOcto

import (
	geo "gitlab.com/Pixdigit/geometry"
	"gitlab.com/Pixdigit/uniqueID"
)

type Positioned interface {
	MoveTo(geo.Point)
	MoveRel(geo.Vec)
}

type Collidable interface {
	CollidesWith(interface{}) (bool, error)
}

type Collection struct {
	Renderables []Renderable
	geo.Location
	id uniqueID.ID
}

func NewCollection(renderables []Renderable) *Collection {
	return &Collection{
		renderables,
		geo.NewLocation(geo.Point{0, 0}),
		uniqueID.NewID(),
	}
}

func EmptyCollection() *Collection {
	return NewCollection(make([]Renderable, 0))
}

func (c *Collection) ID() uniqueID.ID {
	return c.id
}

func (c *Collection) Add(r Renderable) {
	c.Renderables = append(c.Renderables, r)
}

func (c *Collection) Remove(r UniqueRenderable) {
	for i, elem := range c.Renderables {
		switch t := elem.(type) {
		case UniqueRenderable:
			if t.ID() == r.ID() {
				c.Renderables = append(c.Renderables[:i], c.Renderables[i+1:]...)
			}
		}
	}
}

func (c *Collection) MoveTo(p geo.Point) {
	relMotion := c.Location.Pos().Distance(p)

	c.Location.MoveRel(relMotion)

	for _, elem := range c.Renderables {
		switch obj := elem.(type) {
		case Positioned:
			obj.MoveRel(relMotion)
		}
	}
}

func (c *Collection) MoveRel(v geo.Vec) {
	c.Location.MoveRel(v)
	for _, elem := range c.Renderables {
		switch obj := elem.(type) {
		case Positioned:
			obj.MoveRel(v)
		}
	}
}

func (c *Collection) CollidesWith(obj2 Collidable) (bool, error) {
	//Test if any element collides with the Collidable
	//Ignore everything that can not collide
	for _, elem := range c.Renderables {
		switch obj := elem.(type) {
		case Collidable:
			elementCollision, err := obj.CollidesWith(obj2)
			if elementCollision || err != nil {
				return elementCollision, err
			}
		}
	}
	return false, nil
}

func (c *Collection) render() error {
	errs := []error{}
	for _, elem := range c.Renderables {
		err := elem.render()
		errs = append(errs, err)
	}
	if len(errs) >= 1 {
		return errs[0]
	}
	return nil
}
