module gitlab.com/Pixdigit/turboOcto

require (
	github.com/pkg/errors v0.8.0
	github.com/veandco/go-sdl2 v0.0.0-20180925095440-75ff82abc4e3
	gitlab.com/Pixdigit/geometry v0.0.0-20181230184716-d3cb6dbb6c79
	gitlab.com/Pixdigit/goTestTools v0.0.0-20180720152516-ad30e7e0c720
	gitlab.com/Pixdigit/simpleTimer v1.0.0
	gitlab.com/Pixdigit/sorted v0.3.0
	gitlab.com/Pixdigit/uniqueID v1.0.0
	gopkg.in/ini.v1 v1.39.0
)
