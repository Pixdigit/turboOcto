package turboOcto

import (
	"log"

	"github.com/veandco/go-sdl2/sdl"
	geo "gitlab.com/Pixdigit/geometry"
)

type buttonPosition uint8

type mouseState struct {
	geo.Location
	Movement       geo.Vec
	Scroll         geo.Scalar
	ScrollRelative geo.Scalar
	Buttons        map[buttonPosition]*buttonState
}

type mouseButtons struct {
	Left   buttonPosition
	Center buttonPosition
	Right  buttonPosition
	X1     buttonPosition
	X2     buttonPosition
}

var Mouse mouseState
var Buttons mouseButtons

func init() {
	Buttons = mouseButtons{
		Left:   buttonPosition(sdl.BUTTON_LEFT),
		Center: buttonPosition(sdl.BUTTON_MIDDLE),
		Right:  buttonPosition(sdl.BUTTON_RIGHT),
		X1:     buttonPosition(sdl.BUTTON_X1),
		X2:     buttonPosition(sdl.BUTTON_X2),
	}
	Mouse = mouseState{
		geo.NewLocation(geo.Point{0, 0}),
		geo.Vec{0, 0},
		0,
		0,
		map[buttonPosition]*buttonState{
			Buttons.Left:   RELEASED.clone(),
			Buttons.Center: RELEASED.clone(),
			Buttons.Right:  RELEASED.clone(),
			Buttons.X1:     RELEASED.clone(),
			Buttons.X2:     RELEASED.clone(),
		},
	}
}

func (m mouseState) Clicks(obj Collidable) bool {
	//If object can not be collided with, then it cant be clicked
	collision, err := obj.CollidesWith(m.Location)
	if err != nil {
		if Debug {
			log.Println("mouse click detection failed due to unknown error")
		}
		collision = false
	}
	return collision && m.Buttons[Buttons.Left].Is(PRESSING)
}
