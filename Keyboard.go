package turboOcto

import (
	"log"
	"strings"

	"github.com/veandco/go-sdl2/sdl"
)

type keyboardState map[sdl.Scancode]*buttonState

var Keyboard keyboardState

func init() {
	Keyboard = make(map[sdl.Scancode]*buttonState)
}

func (k keyboardState) Pressed(scancodeName string) bool {
	scancodeName = strings.ToLower(scancodeName)
	scancode, ok := scancodeStringMatch[scancodeName]
	if !ok {
		if Debug {
			log.Println("tried to check for keyboard press of unknown name")
		}
		return false
	}
	return k.PressedScanCode(scancode)
}

func (k keyboardState) PressedScanCode(scancode sdl.Scancode) bool {
	keyState, ok := k[scancode]
	if !ok {
		if Debug {
			log.Println("tried to check for keyboard press of unknown sdl.Scancode")
		}
		keyState = RELEASED.clone()
	}
	return keyState.Is(PRESSING)
}
