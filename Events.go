package turboOcto

import (
	"github.com/veandco/go-sdl2/sdl"
	geo "gitlab.com/Pixdigit/geometry"
)

var ExitRequested bool

func updateEvents() error {
	//Reset frame dependend variables
	Mouse.Movement.X = 0
	Mouse.Movement.Y = 0
	Mouse.ScrollRelative = 0
	for i := range Mouse.Buttons {
		Mouse.Buttons[i].changed = false
	}
	for i := range Keyboard {
		Keyboard[i].changed = false
	}

	//Process events
	for event := sdl.PollEvent(); event != nil; event = sdl.PollEvent() {
		switch e := event.(type) {
		case *sdl.MouseButtonEvent:
			newPos := geo.Point{
				geo.Scalar(e.X),
				geo.Scalar(e.Y),
			}
			Mouse.MoveTo(newPos)
			if e.Type == sdl.MOUSEBUTTONDOWN {
				Mouse.Buttons[buttonPosition(e.Button)].update(true)
			} else if e.Type == sdl.MOUSEBUTTONUP {
				Mouse.Buttons[buttonPosition(e.Button)].update(false)
			}
		case *sdl.MouseMotionEvent:
			newPos := geo.Point{
				geo.Scalar(e.X),
				geo.Scalar(e.Y),
			}
			Mouse.MoveTo(newPos)
			Mouse.Movement = geo.Vec{
				geo.Scalar(e.XRel),
				geo.Scalar(e.YRel),
			}
		case *sdl.MouseWheelEvent:
			Mouse.ScrollRelative = geo.Scalar(e.X)
			Mouse.Scroll += geo.Scalar(e.X)
		case *sdl.KeyboardEvent:
			//Create entry for key if it does not exist yet
			_, ok := Keyboard[e.Keysym.Scancode]
			if !ok {
				Keyboard[e.Keysym.Scancode] = RELEASED.clone()
			}
			//Update key
			if e.Type == sdl.KEYDOWN {
				Keyboard[e.Keysym.Scancode].update(true)
			} else if e.Type == sdl.KEYUP {
				Keyboard[e.Keysym.Scancode].update(false)
			}
		case *sdl.QuitEvent:
			ExitRequested = true
		}
	}
	return nil
}
