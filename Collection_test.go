package turboOcto

import (
	"testing"

	geo "gitlab.com/Pixdigit/geometry"
	tools "gitlab.com/Pixdigit/goTestTools"
)

var c *Collection
var f1 *Frame
var f2 *Frame

func TestSetupCollection(t *testing.T) {
	c = EmptyCollection()

	f1, _ = NewEmptyFrame()
	f2, _ = NewEmptyFrame()
	f1.W, f1.H = 2, 2
	f2.W, f2.H = 1, 1

	c.Add(f1)
	c.Add(f2)

	AddElement(c, 1)
}

func TestCollectionMovement(t *testing.T) {
	f1.MoveTo(geo.Point{2, 1})
	f2.MoveTo(geo.Point{3, -1})

	c.MoveTo(geo.Point{-1, -2})
	c.MoveRel(geo.Vec{3, -3})

	tools.Test(f1.Pos() == geo.Point{4, -4}, "sprite did not move with collection", t)
	tools.Test(f2.Pos() == geo.Point{5, -6}, "sprite did not move with collection", t)
	tools.Test(c.Pos() == geo.Point{2, -5}, "collection did not move correctly", t)

}

func TestCollectionCollision(t *testing.T) {
	c.MoveTo(geo.Point{0, 0})
	f1.MoveTo(geo.Point{0, 0})
	f2.MoveTo(geo.Point{-1, -1})

	r := geo.NewRect(geo.Point{-2, -2}, 2, 2)

	ok, err := c.CollidesWith(r)
	if err != nil {
		tools.WrapErr(err, "could not test for collision", t)
	}
	tools.Test(ok, "collision did not occur, when one should have happened", t)
}
