package turboOcto

import (
	"encoding/json"
	"io/ioutil"

	"github.com/pkg/errors"
)

var (
	ConfigPath              string
	AllowFrameSkipping      bool
	DefaultFontRenderMethod renderMethod
	DefaultSpriteTimerMode  timerMode
	FramesVisibleOnLoad     bool
	ResourcePath            string
	SaveOnQuit              bool
	UpdateOnRefresh         bool
	Debug                   bool
)

func initializeConfiguration() error {
	err := SetDefaultConf();	if err != nil {return errors.Wrap(err, "could not initialize configuration with default values")}
	return nil
}

func SetDefaultConf() error {
	quaterSize := screenSize
	quaterSize.W /= 2
	quaterSize.H /= 2

	AllowFrameSkipping = true
	ConfigPath = "./config.json"
	FramesVisibleOnLoad = true
	ResourcePath = "./"
	SaveOnQuit = true
	DefaultFontRenderMethod = BLENDED
	DefaultSpriteTimerMode = USE_FRAME_COUNT
	UpdateOnRefresh = true
	Debug = false

	isFullscreen = true
	grabInput = false
	windowSize = quaterSize
	vRes = quaterSize

	err := updateFromInternalCfg();	if err != nil {return errors.Wrap(err, "could not update based on conf")}
	return nil
}

func LoadConf(filePath string) error {
	data, err := ioutil.ReadFile(filePath);	if err != nil {return errors.Wrap(err, "could not load conf")}
	cfg := &cfgJSON{}
	err = json.Unmarshal(data, cfg);	if err != nil {return errors.Wrap(err, "could not unmarshal data")}
	cfg.sanityCheck()
	cfg.export()

	err = updateFromInternalCfg();	if err != nil {return errors.Wrap(err, "could not update based on conf")}
	return nil
}

func SaveConf() error {
	data, _ := json.MarshalIndent(newCfgJSON(), "", "   ")
	ioutil.WriteFile(ConfigPath, data, 0644)
	return nil
}

func updateFromInternalCfg() error {
	errMsg := "could not process internal variable: "

	if isFullscreen {
		err := Fullscreen();	if err != nil {return errors.Wrap(err, errMsg+"Fullscreen")}
	} else {
		err := Windowed();	if err != nil {return errors.Wrap(err, errMsg+"Fullscreen")}
	}

	window.SetGrab(grabInput)

	err := SetWindowSize(windowSize.W, windowSize.H);	if err != nil {return errors.Wrap(err, errMsg+"WindowSize")}
	err = SetVirtualSize(vRes.W, vRes.H);	if err != nil {return errors.Wrap(err, errMsg+"VirtualSize")}

	return nil
}
